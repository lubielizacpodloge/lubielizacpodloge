from flask import Flask
from flask import render_template

app = Flask(__name__)


@app.route("/")
def hello2():
    return render_template('index.html')
@app.route("/pomoc")
def pomoc():
    return render_template('pomoc.html')
@app.route("/omnie")
def omnie():
    return render_template('omnie.html')
@app.route("/funkcje")
def funkcje():
    my_ip = get_ip()
    remote_host = 'www.amw.gdynia.pl'
    open_port = is_port_open(remote_host,80)
    if open_port == True:
        jaki_port = 'otwarty'
    else:
	    jaki_port = 'zamkniety'
    ip_of_remote_host = socket.gethostbyname(remote_host)
    return render_template('funkcje.html',
	my_ip=my_ip,remote_host=remote_host,  ip_of_remote_host=ip_of_remote_host, jaki_port=jaki_port)
import socket
def get_ip():
    host_name = socket.gethostname()
    ip_address = socket.gethostbyname(host_name)
    return ip_address
def is_port_open(url, port):
	try:
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.settimeout(1)
		s.connect((socket.gethostbyname(url), port))
		s.close()
		return True
	except:
		return False
    
if __name__ == "__main__":
    app.run()
